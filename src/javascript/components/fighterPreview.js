import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  if (fighter) {
    const image = createElement({tagName: 'img', attributes: {'src': fighter.source}});
    fighterElement.appendChild(image);

    const list = createElement({tagName: 'ul'});

    addLineToList(list, `Name: ${fighter.name}`);
    addLineToList(list, `Health: ${fighter.health}`);
    addLineToList(list, `Attack: ${fighter.attack}`);
    addLineToList(list, `Defense: ${fighter.defense}`);

    fighterElement.appendChild(list);
  }
  // todo: show fighter info (image, name, health, etc.)

  return fighterElement;
}

function addLineToList(list, text)
{
  const element = createElement({tagName: 'li'});
  element.innerHTML = text;
  list.appendChild(element)
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
