import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
  });
}

export function getDamage(attacker, defender) {
  return getHitPower(attacker) - getBlockPower(defender);
}

export function getHitPower(fighter) {
  const criticalHitChance = randomBetween(1, 2);
  return fighter.attack * criticalHitChance
}

export function getBlockPower(fighter) {
  const dodgeChance = randomBetween(1, 2);
  return fighter.defense * dodgeChance;
}

function randomBetween(min, max) {
  return Math.floor(
      Math.random() * (max - min) + min
  )
}
